options = {
    username:           'Naomi',
    avatar:             'https://media.discordapp.net/attachments/600712545757822976/970427078275108954/-1.png',
    verified:           true,
    noUser:             false,
    dataSpecified:      false,
    guiTabs:            ['author', 'description'],
    useJsonEditor:      false,
    reverseColumns:     false,
    allowPlaceholders:  false,
    autoUpdateURL:      false,
    autoParams:         false,
    hideEditor:         false,
    hidePreview:        false,
    hideMenu:           false,
}
onload = () => {}
